/*============================================
ACTIVITY

MODULE: WD004-S6-DATA-STRUCTURES-1
GitLab: s6-js-data-structures-1

Challenge:
Swap adjacent (i.e., magkasunod) elements in an arrray if they are in wrong order so 
that the original array will contain the elements/values 1,3,6,9.
Do not use sort() method.


Steps:
1.a. Initialize an array called "arr" with numbers 6, 9, 1, and 3 as its values/elements.
1.b. Declare a variable called tempElement.
1.c. Display arr in the console:
Before: 6,9,1,3


2. Iterate over all the elements in arr so that you can display the ff in your console:
  Compare 6 and 9
  (4)&nbsp;[6, 9, 1, 3]
  Compare 6 and 1
  (4)&nbsp;[6, 9, 1, 3]
  Compare 6 and 3
  (4)&nbsp;[6, 9, 1, 3]
  Compare 9 and 1
  (4)&nbsp;[6, 9, 1, 3]
  Compare 9 and 3
  (4)&nbsp;[6, 9, 1, 3]
  Compare 1 and 3

3. Create a condition so that when the first element in the comparison is greater than the second element, "Swap!" will be displayed. Your console should now display the following:
  Compare 6 and 9
  (4)&nbsp;[6, 9, 1, 3]
  Compare 6 and 1
  Swap!
  (4)&nbsp;[6, 9, 1, 3]
  Compare 6 and 3
  Swap!
  (4)&nbsp;[6, 9, 1, 3]
  Compare 9 and 1
  Swap!
  (4)&nbsp;[6, 9, 1, 3]
  Compare 9 and 3
  Swap!
  (4)&nbsp;[6, 9, 1, 3]
  Compare 1 and 3
  (4)&nbsp;[6, 9, 1, 3]


4. Inside your condition, write a code that will swap the values of the first and second element so that your console would now look like this:
  Compare 6 and 9
  (4)&nbsp;[6,9,1,3]
  Compare 6 and 1
  Swap!
  (4) [1,9,6,3]
  Compare 1 and 3
  (4)&nbsp;[1,9,6,3]
  Compare 9 and 6
  Swap!
  (4)&nbsp;[1,6,9,3]
  Compare 6 and 3
  Swap!
  (4)&nbsp;[1,3,9,6]
  Compare 9 and 6
  Swap!
  (4)&nbsp;[1,3,6,9]

  CLUE: You were asked to declare a tempElement variable earlier. You will be able to use it here to store something... :)

5. Display arr in the console:
After: 1,3,6,9

Stretch Goal:
Enclose your code inside a function and fetch your arr values via prompt.

===============================================*/



/*1.a. Initialize an array called "arr" with numbers 6, 9, 1, and 3 as its values/elements.
1.b. Declare a variable called tempElement.
1.c. Display arr in the console:
Before: 6,9,1,3  */

let arr = [6, 9, 1, 3];
let tempElement;

for(let i=0; i<arr.length; i++) {
  console.log(arr[i]);
}

/*2. Iterate over all the elements in arr so that you can display the ff in your console:
  Compare 6 and 9
  (4) [6, 9, 1, 3]
  Compare 6 and 1
  (4) [6, 9, 1, 3]
  Compare 6 and 3
  (4) [6, 9, 1, 3]
  Compare 9 and 1
  (4) [6, 9, 1, 3]
  Compare 9 and 3
  (4) [6, 9, 1, 3]
  Compare 1 and 3 

or
  6 9 
  6 1
  6 3
  9 1
  9 3
  1 3
   */

let x = 0;
console.log(arr);
  for(i=0; i<arr.length; i++) {
    x = i;
    while(x !== arr.length-1) {
      console.log(arr[i] + " " + arr[x+1]);
      //console.log(arr);
      x++;
    }
  }


/*3. Create a condition so that when the first element in the comparison is greater than the second element, "Swap!" will be displayed. Your console should now display the following:
  Compare 6 and 9       first
  (4) [6, 9, 1, 3]
  Compare 6 and 1       second
  Swap!
  (4) [6, 9, 1, 3]
  Compare 6 and 3       third
  Swap!
  (4) [6, 9, 1, 3]
  Compare 9 and 1       fourth
  Swap!
  (4) [6, 9, 1, 3]
  Compare 9 and 3       fifth
  Swap!
  (4) [6, 9, 1, 3]
  Compare 1 and 3       sixth
  (4) [6, 9, 1, 3]

  6 9
  6 1
  Swap!
  6 3
  Swap!
  9 1
  Swap!
  9 3
  Swap!
  1 3

  */

console.log(arr);
  for(i=0; i<arr.length; i++) {
    x = i;
    while(x !== arr.length-1) {      
      console.log(arr[i] + " " + arr[x+1]);
      //console.log(arr);
      if(arr[i] > arr[x+1]) {
        console.log("Swap!");
      }
      x++;
    }
  }


/*4. Inside your condition, write a code that will swap the values of the first and second element so that your console would now look like this:
  Compare 6 and 9     first
  (4) [6,9,1,3]
  Compare 6 and 1      second
  Swap!
  (4) [1,9,6,3]
  Compare 1 and 3      third
  (4) [1,9,6,3]
  Compare 9 and 6       fourth
  Swap!
  (4) [1,6,9,3]
  Compare 6 and 3       fifth
  Swap!
  (4) [1,3,9,6]
  Compare 9 and 6       sixth
  Swap!
  (4) [1,3,6,9]


  6 9
  [6 9 1 3]

  6 1 Swap!
  [1 9 6 3]

  1 3
  9 6 Swap!

  [1 6 9 3]
  6 3 Swap!

  [1 3 9 6]
  9 6 Swap!

  [1 3 6 9]


  CLUE: You were asked to declare a tempElement variable earlier. You will be able to use it here to store something... :)  */


console.log(arr);
  for(i=0; i<arr.length; i++) {
    x = i;
    while(x !== arr.length-1) {      
      console.log(arr[i] + " " + arr[x+1]);
      if(arr[i] > arr[x+1]) {
        tempElement = arr[x+1];
        arr[x+1] = arr[i];
        arr[i] = tempElement;
        console.log("Swap!");
        console.log(arr);
      }
      x++;
    }
  }



/*5. Display arr in the console:
After: 1,3,6,9   */
console.log(arr);


/*Stretch Goal:
Enclose your code inside a function and fetch your arr values via prompt.  */


let numbersToSort = prompt("Enter numbers to be sorted. Add comma in between them.");

let numbersToSort2 = numbersToSort.split(",");


for(i=0; i<numbersToSort2.length; i++) {
  numbersToSort2[i] = parseInt(numbersToSort2[i]);
}

console.log(numbersToSort2);

function sortTheArray(numbersToSort2) {
  for(i=0; i<numbersToSort2.length; i++) {
    x = i;
    while(x !== numbersToSort2.length-1) {      
      console.log(numbersToSort2[i] + " " + numbersToSort2[x+1]);
      if(numbersToSort2[i] > numbersToSort2[x+1]) {
        tempElement = numbersToSort2[x+1];
        numbersToSort2[x+1] = numbersToSort2[i];
        numbersToSort2[i] = tempElement;
        console.log("Swap!");
        console.log(numbersToSort2);
      }
      x++;
    }
  }
}

sortTheArray(numbersToSort2);
console.log(numbersToSort2);

/*===============================================*/