
//sort

let answer = prompt("Enter numbers to be sorted. Add comma in between them.");

console.log(`${answer} is a ${typeof answer}`);
console.log(`${answer} has ${answer.length} elements.`);
console.log("************************************");

let answer2 = answer.split(",");
console.log(`Using the split method, ${answer} now has ${answer2.length} elements.`);
console.log(`It's elements are still type ${typeof answer2[0]}.`);
console.log(`${answer2} will be sorted to ${answer2.sort()}.`);
answer2[0] = parseInt(answer2[0]);
console.log(`So I need to convert the elements into ${typeof answer2[0]} using parseInt before I sort them.`);



