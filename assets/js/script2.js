/* ==================================

ACTIVITY

MODULE: WD004-S5-MULTI-DEMINSIONAL ARRAYS & OBJECTS
GitLab: s5-js-array-objects-2

1. Create a zodiacSigns array */
let zodiacSigns = [];


/* 2. The first element of the zodiacSigns array is an empty string (i.e., "") while  
the rest are objects with properties: month, border, and signs. 
Signs is a nested array with two elements.
 - January
    - border: 20
    - signs: Aquarius and Capricorn
 - February
    - border: 19
    - signs: Pisces and Aquarius
 - March
     - border: 21
    - signs: Aries and Pisces
 - April
     - border: 20
    - signs: Taurus and Aries
 - May
    - border: 21
    - signs: Gemini and Taurus
 - June
    - border: 21
    - signs: Cancer and Gemini
 - July
    - border: 23
    - signs: Leo and Cancer
 - August
    - border: 23
    - signs: Virgo and Leo
 - September
    - border: 23
    - signs: Libra and Virgo
 - October
    - border: 23
    - signs: Scorpio and Libra
 - November 
    - border: 22
    - signs: Saggitarius and Scorpio
 - December 
    - border: 22
    - signs: Capricorn and Saggitarius
    */

zodiacSigns = [ "",
                {
                  month: "January",
                  border: 20,
                  signs: [ "Aquarius", "Capricorn"]
                },
                {
                  month: "February",
                  border: 19,
                  signs: [ "Pisces", "Aquarius"]
                },
                {
                  month: "March",
                  border: 21,
                  signs: [ "Aries", "Pisces"]
                },
                {
                  month: "March",
                  border: 21,
                  signs: [ "Aries", "Aquarius"]
                },
                {
                  month: "April",
                  border: 20,
                  signs: [ "Taurus", "Aries"]
                },
                {
                  month: "May",
                  border: 21,
                  signs: [ "Gemini", "Taurus"]
                },
                {
                  month: "June",
                  border: 21,
                  signs: [ "Cancer", "Gemini"]
                },
                {
                  month: "July",
                  border: 23,
                  signs: [ "Leo", "Cancer"]
                },
                {
                  month: "August",
                  border: 23,
                  signs: [ "Virgo", "Leo"]
                },
                {
                  month: "September",
                  border: 23,
                  signs: [ "Libra", "Virgo"]
                },
                {
                  month: "October",
                  border: 23,
                  signs: [ "Scorpio", "Libra"]
                },
                {
                  month: "November",
                  border: 22,
                  signs: [ "Saggitarius", "Scorpio"]
                },
                {
                  month: "December",
                  border: 22,
                  signs: [ "Capricorn", "Saggitarius"]
                }
]


/*3. Create the variables monthInput and dayInput and get their values via prompt.
Note: The value of both dayInput and monthInput are numbers.*/

let monthInput = prompt("What is your birth month?");
let dayInput = prompt ("What is your birth 'date' (day of the month)?");


/*4. Create a function that checks if the value of dayInput is greater than or 
equal to the border of monthInput (i.e., date in the month when the zodiac sign will change). 
If the dayInput is at or past the border, display the second sign, else display the 
preceding sign. Your expected output should display this:
- Your zodiac sign is _________.*/

function determineZodiac(monthInput, dayInput) {
  for(let i=0; i < zodiacSigns.length; i++) {
    if(monthInput === zodiacSigns[i].month) {
      if(dayInput >= zodiacSigns[i].border) {
        console.log(`Your zodiac sign is ${zodiacSigns[i].signs[1]}.`);
      } else {
        console.log(`Your zodiac sign is ${zodiacSigns[i].signs[0]}.`);
      }
    }
  }
}

determineZodiac(monthInput, dayInput);

/*STRETCH GOAL:
1. monthInput is a word but is converted into a number via Switch Case so that the 
ff is displayed instead:
- You were born in NOVEMBER 17. Your zodiac sign is _________.*/

switch (monthInput.toLowerCase()) {
  case 'january':
          monthInput = 1;
          break;
  case 'february':
          monthInput = 2;
          break;
  case 'march':
          monthInput = 3;
          break;
  case 'april':
          monthInput = 4;
          break;
  case 'may':
          monthInput = 5;
          break;
  case 'june':
          monthInput = 6;
          break;
  case 'july':
          monthInput = 7;
          break;
  case 'august':
          monthInput = 8;
          break;
  case 'september':
          monthInput = 9;
          break;
  case 'october':
          monthInput = 10;
          break;
  case 'november':
          monthInput = 11;
          break;
  case 'december':
          monthInput = 12;
          break;
  default:
          console.log(`Sorry. Zodia sign for ${monthInput} doesn't exist.`)
}

function determineZodiac(monthInput, dayInput) {
  for(let i=0; i < zodiacSigns.length; i++) {
    if(monthInput === zodiacSigns[i].month) {
      if(dayInput >= zodiacSigns[i].border) {
        console.log(`Your zodiac sign is ${zodiacSigns[i].signs[1]}.`);
      } else {
        console.log(`Your zodiac sign is ${zodiacSigns[i].signs[0]}.`);
      }
    }
  }
}

determineZodiac(monthInput, dayInput);


/*2. If monthInput is wrong, the ff message is displayed instead:
- Sorry. Zodia sign for ______________ doesn't exist. */

function determineZodiac(monthInput, dayInput) {
  for(let i=0; i < zodiacSigns.length; i++) {
    if(monthInput === zodiacSigns[i].month) {
      if(dayInput >= zodiacSigns[i].border) {
        console.log(`Your zodiac sign is ${zodiacSigns[i].signs[1]}.`);
      } else {
        console.log(`Your zodiac sign is ${zodiacSigns[i].signs[0]}.`);
      }
    }
  }
}

determineZodiac(monthInput, dayInput);



/*3. Signs is a nested array of two objects with properties: name and horoscope.
  horoscope is a METHOD that returns the following messages depending on the sign:
  - Deep and Creative Aquarius 
  - Serious and Strong Capricorn
  - Wise and Artistic Pisces
  - Eager and Quick Aries
  - Strong and Reliable Taurus
  - Curious and Kind Gemini
  - Sensitive and Loyal Cancer
  - Fiery and Confident Leo
  - Gentle and Smart Virgo
  - Sociable and Fair Libra
  - Original and Brave Scorpio
  - Funny and Generous Saggitarius
  - Serious and Strong Capricorn */





/*================================*/
//s6-js-data-structures-1